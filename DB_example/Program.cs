﻿using DB_example.Repository;
using MySqlConnector;

internal class Program
{
    private static async Task Main(string[] args)
    {
        var builder = new MySqlConnectionStringBuilder()
        {
            Server = "localhost",
            Database = "my_schema",
            UserID = "root",
            Password = "qwerty123"
        };

        using (var db = new MyDbContext())
        {
            var persons = db.Persons.Where(p => p.City.Contains("Mogilev")).FirstOrDefault();

        }
        //using (var sqlConnect = new MySqlConnection(builder.ConnectionString))
        //{
        //    await sqlConnect.OpenAsync();
        //    using (var sqlQuery = sqlConnect.CreateCommand())
        //    {
        //        sqlQuery.CommandText = "SELECT * from persons";
        //        using (MySqlDataReader reader = sqlQuery.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                var lastName = reader.GetString("LastName");
        //            }
        //        }
        //    }
        //};
        Console.WriteLine("Hello, World!");
    }
}