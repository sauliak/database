﻿using DB_example.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_example.Repository
{
    internal class MyDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new MySqlConnectionStringBuilder()
            {
                Server = "localhost",
                Database = "my_schema",
                UserID = "root",
                Password = "qwerty123"
            };
            optionsBuilder.UseMySql(builder.ConnectionString, ServerVersion.AutoDetect(builder.ConnectionString));
            //base.OnConfiguring(optionsBuilder);
        }
    }
}
